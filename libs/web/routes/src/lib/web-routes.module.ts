import {NgModule} from '@angular/core';
import {Route, RouterModule} from "@angular/router";

const routes: Route[] = [
  {
    path: '',
    pathMatch: 'full',
    redirectTo: 'calendar-scheduler'
  },
  {
    path: 'calendar-scheduler',
    loadChildren:
      '@angular-scheduler/calendar/scheduler#CalendarSchedulerModule'
  }
];

@NgModule({
  imports: [
    RouterModule.forRoot(routes, {initialNavigation: 'enabled'}
    ),
  ],
  exports: [RouterModule]
})
export class RoutesModule {
}
