import { async, TestBed } from '@angular/core/testing';
import { WebRoutesModule } from './web-routes.module';

describe('WebRoutesModule', () => {
  beforeEach(async(() => {
    TestBed.configureTestingModule({
      imports: [WebRoutesModule]
    }).compileComponents();
  }));

  it('should create', () => {
    expect(WebRoutesModule).toBeDefined();
  });
});
