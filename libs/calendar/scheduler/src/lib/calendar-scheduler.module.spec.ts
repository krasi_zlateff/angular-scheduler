import { async, TestBed } from '@angular/core/testing';
import { CalendarSchedulerModule } from './calendar-scheduler.module';

describe('CalendarSchedulerModule', () => {
  beforeEach(async(() => {
    TestBed.configureTestingModule({
      imports: [CalendarSchedulerModule]
    }).compileComponents();
  }));

  it('should create', () => {
    expect(CalendarSchedulerModule).toBeDefined();
  });
});
