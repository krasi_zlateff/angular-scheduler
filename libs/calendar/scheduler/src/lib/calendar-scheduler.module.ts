import {NgModule} from '@angular/core';
import {CommonModule} from '@angular/common';
import {RouterModule} from '@angular/router';
import {SchedulerComponent} from './containers/scheduler/scheduler.component';
import {NgbModalModule, NgbModule} from "@ng-bootstrap/ng-bootstrap";
import {FormsModule} from "@angular/forms";
import {FlatpickrModule} from "angularx-flatpickr";
import {CalendarModule, DateAdapter} from "angular-calendar";
import {adapterFactory} from "angular-calendar/date-adapters/date-fns";
import {HeaderComponent} from './containers/header/header.component';
import {BodyComponent} from './containers/body/body.component';
import {FooterComponent} from './containers/footer/footer.component';
import {SchedulerService} from "./services/scheduler.service";
import {HttpClientModule} from "@angular/common/http";

const COMPONENTS = [
  SchedulerComponent
  , HeaderComponent
  , BodyComponent
  , FooterComponent
];

const PROVIDERS = [
  SchedulerService
];

@NgModule({
  imports: [
    CommonModule,
    HttpClientModule,
    RouterModule.forChild([
      {path: '', pathMatch: 'full', component: SchedulerComponent}
    ]),
    FormsModule,
    NgbModule,
    NgbModalModule,
    FlatpickrModule.forRoot(),
    CalendarModule.forRoot({
      provide: DateAdapter,
      useFactory: adapterFactory
    })

  ],
  declarations: [COMPONENTS],
  providers: [PROVIDERS],
  exports: [COMPONENTS]
})
export class CalendarSchedulerModule {
}
