import {AfterViewInit, ChangeDetectionStrategy, Component, OnInit} from '@angular/core';
import {CalendarEvent} from "angular-calendar";
import {Observable} from "rxjs";
import {
    endOfDay,
    endOfMonth,
    endOfWeek,
    format,
    isSameDay,
    isSameMonth,
    startOfDay,
    startOfMonth,
    startOfWeek
} from "date-fns";
import {HttpParams} from "@angular/common/http";
import {SchedulerService} from "../../services/scheduler.service";
import {map} from "rxjs/operators";

interface Film {
    id: number;
    title: string;
    release_date: string;
}

@Component({
    selector: 'angular-scheduler-body',
    templateUrl: './body.component.html',
    changeDetection: ChangeDetectionStrategy.OnPush,
    styleUrls: ['./body.component.scss']
})
export class BodyComponent implements OnInit, AfterViewInit {

    view = 'month';

    viewDate: Date;

    events$: Observable<Array<CalendarEvent<{ film: Film }>>>;

    activeDayIsOpen = false;

    constructor(
        private schedulerService: SchedulerService
    ) {
    }

    ngOnInit() {
        this.getMoviePrimaryReleases();
    }

    ngAfterViewInit(): void {

    }

    getMoviePrimaryReleases(): void {
        const getStart: any = {
            month: startOfMonth,
            week: startOfWeek,
            day: startOfDay
        }[this.view];

        const getEnd: any = {
            month: endOfMonth,
            week: endOfWeek,
            day: endOfDay
        }[this.view];

        const params = new HttpParams()
            .set(
                'primary_release_date.gte',
                format(getStart(this.schedulerService.viewDate), 'YYYY-MM-DD')
            )
            .set(
                'primary_release_date.lte',
                format(getEnd(this.schedulerService.viewDate), 'YYYY-MM-DD')
            )
            .set('api_key', '0fb0c4d40252644da4a0755d4dc5bb24');

        this.events$ = this.schedulerService.getMoviePrimaryReleases(params);
        this.viewDate = this.schedulerService.viewDate;
    }

    dayClicked({date, events}: {
        date: Date;
        events: Array<CalendarEvent<{ film: Film }>>;
    }): void {
        if (isSameMonth(date, this.viewDate)) {
            if (
                (isSameDay(this.viewDate, date) && this.activeDayIsOpen === true) ||
                events.length === 0
            ) {
                this.activeDayIsOpen = false;
            } else {
                this.activeDayIsOpen = true;
                this.viewDate = date;
            }
        }
    }

    eventClicked(event: CalendarEvent<{ film: Film }>): void {
        window.open(
            `https://www.themoviedb.org/movie/${event.meta.film.id}`,
            '_blank'
        );
    }

}
