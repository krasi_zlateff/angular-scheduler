import {ChangeDetectionStrategy, Component, OnInit} from '@angular/core';
import {CalendarView} from "angular-calendar";

@Component({
  selector: 'angular-scheduler-header',
  changeDetection: ChangeDetectionStrategy.OnPush,
  templateUrl: './header.component.html',
  styleUrls: ['./header.component.scss']
})
export class HeaderComponent implements OnInit {

  CalendarView = CalendarView;

  view: CalendarView = CalendarView.Month;

  viewDate: Date = new Date();

  activeDayIsOpen = true;

  constructor() {
  }

  ngOnInit() {
  }

}
