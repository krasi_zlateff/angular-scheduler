import {Injectable} from '@angular/core';
import {HttpClient, HttpParams} from "@angular/common/http";
import {Observable, of} from "rxjs";
import {catchError, map} from "rxjs/operators";

interface Film {
    id: number;
    title: string;
    start: Date;
    release_date: string;
}

const colors: any = {
    red: {
        primary: '#ad2121',
        secondary: '#FAE3E3'
    },
    blue: {
        primary: '#1e90ff',
        secondary: '#D1E8FF'
    },
    yellow: {
        primary: '#e3bc08',
        secondary: '#FDF1BA'
    }
};


function getTimezoneOffsetString(date: Date): string {
    const timezoneOffset = date.getTimezoneOffset();
    const hoursOffset = String(
        Math.floor(Math.abs(timezoneOffset / 60))
    ).padStart(2, '0');
    const minutesOffset = String(Math.abs(timezoneOffset % 60)).padEnd(2, '0');
    const direction = timezoneOffset > 0 ? '-' : '+';

    return `T00:00:00${direction}${hoursOffset}:${minutesOffset}`;
}

@Injectable()
export class SchedulerService {

    viewDate: Date = new Date();

    private moviesUrl = 'https://api.themoviedb.org/3/discover/movie';

    constructor(
        private http: HttpClient
    ) {
    }

    getMoviePrimaryReleases(params): Observable<any> {
        return this.http
            .get(this.moviesUrl, {params})
            .pipe(
                map(({results}: { results: Film[] }) => {
                    return results.map((film: Film) => {
                        return {
                            title: film.title,
                            start: new Date(
                                film.release_date + getTimezoneOffsetString(this.viewDate)
                            ),
                            color: colors.yellow,
                            allDay: true,
                            meta: {
                                film
                            }
                        };
                    });
                }),
                catchError(this.handleError<Film[]>('getMoviePrimaryReleases', []))
            );
    }

    handleError<T>(operation = 'operation', result?: T) {
        return (error: any): Observable<T> => {

            console.error(error);

            return of(result as T);
        }
    }
}
